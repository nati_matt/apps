/*
 * Copyright 2017-present Open Networking Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.tesissdn.igmp.api;

import com.fasterxml.jackson.databind.node.ObjectNode;

import org.onosproject.net.mcast.MulticastRouteService;
import org.onosproject.rest.AbstractWebResource;
import org.slf4j.Logger;


import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Sinks Information.
 */
@Path("sinks")
public class SinksWebResource extends AbstractWebResource {

    private final Logger log = getLogger(getClass());

    /**
     * Get all multicast routes.
     *
     * @return 200 OK
     */

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("data")
    public Response postSourcesEnabled() {

        MulticastRouteService multicastRouteService = get(MulticastRouteService.class);
        ObjectNode node = mapper().createObjectNode();
        multicastRouteService.getRoutes().forEach(mcastRoute -> {
            node.put(mcastRoute.group().toString(), mcastRoute.source().toString());
        });
        return ok(node).build();
    }
}
