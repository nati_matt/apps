/*
 * Copyright 2017-present Open Networking Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.tesissdn.igmp.api;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.onlab.packet.IpAddress;
import org.onosproject.net.mcast.McastRoute;
import org.onosproject.net.mcast.MulticastRouteService;
import org.onosproject.rest.AbstractWebResource;
import org.slf4j.Logger;
import org.tesissdn.igmp.IgmpSnoopService;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Sources Information.
 */
@Path("sources")
public class SourcesWebResource extends AbstractWebResource {

    private final Logger log = getLogger(getClass());

    /**
     * Get enabled hosts.
     *
     * @return JSON enabled sources
     */

    @GET
    @Path("enabled")
    public Response getSourcesEnabled() {
        IgmpSnoopService igmpSnoopService = get(IgmpSnoopService.class);
        
        ObjectNode node = mapper().createObjectNode();
        ObjectNode value = mapper().createObjectNode();
        ArrayNode array = mapper().createArrayNode();
        
        igmpSnoopService.getSsmTranslateTable().entrySet().forEach(entry -> {
            
            value.put("group", entry.getKey().toString());
            
            array.removeAll();
            entry.getValue().forEach(ip -> array.add(ip.toString()));            
            value.set("sources", array);
        });
        
        node.set("sourcesEnabled", value);

        return ok(node).build();
    }

    /**
     * Set enabled hosts.
     *
     * @param group multicast group
     * @param source source to add
     * @return 200 OK or 500 on error
     */

    @POST
    @Path("enabled/{group}/{source}")
    @Consumes(MediaType.APPLICATION_JSON)
    // @Produces(MediaType.APPLICATION_JSON)
    public Response postSourcesEnabled(@PathParam("group") String group,
                                       @PathParam("source") String source) {
        IgmpSnoopService igmpSnoopService = get(IgmpSnoopService.class);
        List<McastRoute> translations = new ArrayList<>();

        log.info("GROUP {} SOURCE {}", group, source);

        IpAddress ipSource = IpAddress.valueOf(source.trim());
        IpAddress ipGroup = IpAddress.valueOf(group.trim());

        translations.add(new McastRoute(ipSource, ipGroup, McastRoute.Type.STATIC));

        igmpSnoopService.addToSsmTranslateTable(translations);

        return Response.ok().build();
    }
}
