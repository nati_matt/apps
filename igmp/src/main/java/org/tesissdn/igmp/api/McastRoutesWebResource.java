/*
 * Copyright 2017-present Open Networking Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.tesissdn.igmp.api;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.onlab.packet.IpAddress;

import org.onosproject.net.ConnectPoint;

import org.onosproject.net.host.HostService;
import org.onosproject.net.mcast.MulticastRouteService;
import org.onosproject.rest.AbstractWebResource;
import org.slf4j.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Multicast Routes Information.
 */
@Path("mcast/routes")
public class McastRoutesWebResource extends AbstractWebResource {

    private final Logger log = getLogger(getClass());
    private static final String SOURCE = "source";
    private static final String GROUP = "group";

    /**
     * Get all multicast routes.
     *
     * @return 200 OK
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMcastRoutes() {
        MulticastRouteService multicastRouteService = get(MulticastRouteService.class);
        ObjectNode node = mapper().createObjectNode();
        ArrayNode nodeArray = mapper().createArrayNode();

        multicastRouteService.getRoutes().forEach(mcastRoute -> {
            ObjectNode routeNode = mapper().createObjectNode();
            ArrayNode routeArray = mapper().createArrayNode();
            multicastRouteService.fetchSinks(mcastRoute).forEach(sink -> {
                try {
                    routeArray.add(getHostIP(sink).toString());

                } catch (Exception e) {
                    log.warn(e.toString());
                }
            });
            routeNode.put("group", mcastRoute.group().toString());
            routeNode.put("source", mcastRoute.source().toString());
            routeNode.putArray("sinks").addAll(routeArray);
            nodeArray.add(routeNode);
        });

        node.putArray("mcastRoutes").addAll(nodeArray);
        return Response.ok(node).build();
    }

    private IpAddress getHostIP(ConnectPoint cp) {
        HostService hostService = get(HostService.class);
        return hostService.getConnectedHosts(cp).iterator().next().ipAddresses().iterator().next();
    }

}
