/*
 * Copyright 2015-present Open Networking Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.tesissdn.igmp;

import org.onlab.packet.Ethernet;
import org.onosproject.core.ApplicationId;
import org.onosproject.net.ConnectPoint;
import org.onosproject.net.flow.DefaultTrafficSelector;
import org.onosproject.net.flow.DefaultTrafficTreatment;
import org.onosproject.net.flow.TrafficSelector;
import org.onosproject.net.flow.TrafficTreatment;
import org.onosproject.net.intent.Intent;
import org.onosproject.net.intent.IntentService;
import org.onosproject.net.intent.Key;
import org.onosproject.net.intent.SinglePointToMultiPointIntent;
import org.onosproject.net.mcast.McastEvent;
import org.onosproject.net.mcast.McastListener;
import org.onosproject.net.mcast.McastRoute;
import org.onosproject.net.mcast.MulticastRouteService;
import org.slf4j.Logger;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.slf4j.LoggerFactory.getLogger;

public class McastIntentManager implements McastListener {

    private final Logger log = getLogger(getClass());

    private Map<McastRoute, Key> intentHashMap;

    private ApplicationId appId;

    private IntentService intentService;

    private MulticastRouteService multicastService;

    private InternalIntentListener internalIntentListener;

    public McastIntentManager(IntentService intentService,
                              ApplicationId appId, MulticastRouteService multicastService) {

        this.appId = appId;
        this.intentService = intentService;
        this.internalIntentListener = new InternalIntentListener(intentService);
        intentService.addListener(internalIntentListener);
        this.multicastService = multicastService;
        multicastService.addListener(this);
        intentHashMap = new HashMap<>();
    }

    @Override
    public void event(McastEvent event) {
        McastRoute route = event.subject().route();
        Key oldKey;

        log.info("-------------- MULTICAST EVENT --------------");
        log.info(event.toString());

        Set<ConnectPoint> egressPoints = new HashSet<>(multicastService.fetchSinks(route));

        if (event.type() != McastEvent.Type.SOURCE_ADDED &&
                event.type() != McastEvent.Type.SINK_REMOVED &&
                event.type() != McastEvent.Type.ROUTE_REMOVED &&
                event.type() != McastEvent.Type.SINK_ADDED) {
            return;
        }

        if (intentHashMap.containsKey(route)) {
            oldKey = intentHashMap.get(route);
            withdrawIntent(oldKey);

            log.info("-------------- INTENT REMOVED --------------");
        } else {
            oldKey = null;
        }

        if (!egressPoints.isEmpty()) {

            SinglePointToMultiPointIntent intent = setIntent(route);

            if (oldKey != null) {

                internalIntentListener.addIntentToInstall(oldKey, intent);
            } else {
                intentService.submit(intent);
                log.info("-------------- INTENT ADDED -------------- 1");
            }

            intentHashMap.put(route, intent.key());
        }
    }

    public void deactivate() {
        withdrawAllIntents();
        intentService.removeListener(internalIntentListener);
        multicastService.removeListener(this);

    }

    private SinglePointToMultiPointIntent setIntent(McastRoute route) {

        ConnectPoint ingressPoint = multicastService.fetchSource(route);
        Set<ConnectPoint> egressPoints = new HashSet<>(multicastService.fetchSinks(route));

        TrafficSelector.Builder selector = DefaultTrafficSelector.builder();
        TrafficTreatment treatment = DefaultTrafficTreatment.emptyTreatment();

        if (ingressPoint == null || egressPoints.isEmpty()) {
            log.warn("Can't set intent without an ingress or egress connect points");
            log.info("ingress point {}", ingressPoint);
            log.info("egress point {}", egressPoints.toString());
            return null;
        }

        selector.matchEthType(Ethernet.TYPE_IPV4)
                .matchIPDst(route.group().toIpPrefix())
                .matchIPSrc(route.source().toIpPrefix());

        SinglePointToMultiPointIntent.Builder builder = SinglePointToMultiPointIntent.builder()
                .appId(appId)
                .selector(selector.build())
                .treatment(treatment)
                .ingressPoint(ingressPoint)
                .egressPoints(egressPoints);


        SinglePointToMultiPointIntent intent;
        intent = builder.build();

        return intent;
    }

    public void withdrawAllIntents() {
        for (Map.Entry<McastRoute, Key> entry : intentHashMap.entrySet()) {
            withdrawIntent(entry.getValue());
        }
        internalIntentListener.clearTable();
        intentHashMap.clear();
    }

    public void withdrawIntent(Key key) {
        if (key == null) {
            // nothing to withdraw
            return;
        }
        Intent intent = intentService.getIntent(key);

        intentService.withdraw(intent);
    }

    public Map<McastRoute, Key> getIntentHashMap() {
        return intentHashMap;
    }

    public void reComputeIntents() {
        intentHashMap.values().forEach(intentKey -> {
            Intent intent = intentService.getIntent(intentKey);
            intentService.withdraw(intent);
            intentService.submit(intent);
        });
    }

//    public void re
}

