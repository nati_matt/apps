/*
 * Copyright 2015-present Open Networking Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.tesissdn.igmp;

import org.onlab.packet.IpAddress;
import org.onosproject.core.ApplicationId;
import org.onosproject.net.ConnectPoint;
import org.onosproject.net.flow.FlowRuleService;
import org.onosproject.net.flowobjective.FlowObjectiveService;
import org.onosproject.net.host.HostService;
import org.onosproject.net.mcast.McastRoute;
import org.onosproject.net.mcast.MulticastRouteService;
import org.slf4j.Logger;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import static org.slf4j.LoggerFactory.getLogger;

public class MulticastData {

    private final Logger log = getLogger(getClass());


    protected FlowRuleService flowRuleService;
    protected FlowObjectiveService flowObjectiveService;
    protected HostService hostService;


    private Map<IpAddress, Set<IpAddress>> ssmTranslateTable = new ConcurrentHashMap<>();

    private Map<IpAddress, ConnectPoint> sourceConnectTable = new ConcurrentHashMap<>();

    private Map<Channel, Set<ConnectPoint>> sinkConnectTable = new ConcurrentHashMap<>();

    private Map<Channel, Set<IpAddress>> sinkBlockedTable = new ConcurrentHashMap<>();

    private MulticastRouteService multicastService;

    private ApplicationId appId;

    public MulticastData(MulticastRouteService multicastService, ApplicationId appId,
                         FlowRuleService flowRuleService, FlowObjectiveService flowObjectiveService,
                         HostService hostService) {
        this.flowRuleService = flowRuleService;
        this.multicastService = multicastService;
        this.appId = appId;
        this.flowObjectiveService = flowObjectiveService;
        this.hostService = hostService;
    }

    public void saveSinks(Channel channel, ConnectPoint location) {

        if (channel.source == null) {
            Set<IpAddress> sourceSet = ssmTranslateTable.get(channel.group);

            if (sourceSet != null) {
                channel.source = sourceSet.iterator().next();
            } else {
                return;
            }
        }
        Boolean test;
        try {
            test = sinkBlockedTable.get(channel).contains(getHostIP(location));
        } catch (Exception e) {
            test = false;
        }
        if (test) {
            log.info("SAVE SINK BLOQUEADA");
            return;
        }


        Set<ConnectPoint> prevSinks = sinkConnectTable.get(channel);

        if (prevSinks == null) {
            prevSinks = new HashSet<>();
        }
        if (prevSinks.contains(location)) {
            return;
        }

        prevSinks.add(location);

        log.info("Save IGMP packet, group ({}) -> sink port: {}", channel.group, location);
        sinkConnectTable.put(channel, prevSinks);
    }

    public void removeSinks(Channel channel, ConnectPoint location) {
//
        log.info("Remove IGMP packet, group ({}) -> sink port: {}", channel.group, location);

        if (channel.source == null) {
            sinkConnectTable.forEach((chan, setCp) -> {
                if (setCp.contains(location)) {
                    setCp.remove(location);
                    sinkConnectTable.put(chan, setCp);
                    McastRoute mcastRoute = new McastRoute(chan.source, chan.group, McastRoute.Type.IGMP);
                    multicastService.removeSink(mcastRoute, location);
                }
            });

        } else  {
            McastRoute mcastRoute = new McastRoute(channel.source, channel.group, McastRoute.Type.IGMP);

            Set<ConnectPoint> prevSinks = new HashSet<>();
            multicastService.getRoutes().forEach(mcastRoute1 -> {
                if (mcastRoute.equals(mcastRoute1)) {
                    prevSinks.addAll(multicastService.fetchSinks(mcastRoute1));
                    log.info("SINKS {}", multicastService.fetchSinks(mcastRoute1));
                }
            });

            if (prevSinks.isEmpty()) {
                return;
            }

            if (!prevSinks.contains(location)) {
                return;
            }

            multicastService.getRoutes().forEach(mcastRoute1 -> {
                if (mcastRoute.equals(mcastRoute1)) {
                    multicastService.removeSink(mcastRoute1, location);
                }
            });
            prevSinks.remove(location);
            sinkConnectTable.put(channel, prevSinks);
        }

    }

    public void saveSource(Channel channel, ConnectPoint location) {

        if (!ssmTranslateTable.containsKey(channel.group)) {
            log.info("Invalid group");
            return;
        }
        Set<IpAddress> sources = ssmTranslateTable.get(channel.group);
        if (!sources.contains(channel.source)) {
            log.info("Invalid source for multicast group {}", channel.group.getIp4Address().toString());
            return;
        }

        log.info("Save source, ip ({}) -> port: {}", channel.group.toIpPrefix(), location);
//
        sourceConnectTable.put(channel.source, location);
    }

    public void mergeSourceSinksTable() {

        sinkConnectTable.forEach((channel, setSink) -> {
            if (sourceConnectTable.containsKey(channel.source)) {
                McastRoute mcastRoute = new McastRoute(channel.source, channel.group, McastRoute.Type.IGMP);

                if (multicastService.getRoutes().contains(mcastRoute)) {
                    if (multicastService.fetchSinks(mcastRoute).hashCode() != setSink.hashCode()) {
                        log.info("Mcast route modificada");
                        setSink.forEach(sink -> multicastService.addSink(mcastRoute, sink));
                        multicastService.addSource(mcastRoute, sourceConnectTable.get(channel.source));
                    }
                } else {
                    setSink.forEach(sink -> multicastService.addSink(mcastRoute, sink));
                    multicastService.addSource(mcastRoute, sourceConnectTable.get(channel.source));
                }
                multicastService.add(mcastRoute);
            }
        });
    }

    public void clearTables() {
        sourceConnectTable.clear();
        sinkConnectTable.clear();

        flowRuleService.removeFlowRulesById(appId);

        multicastService.getRoutes().forEach(route -> multicastService.remove(route));

    }

    public boolean sourceExists(IpAddress group) {
        return sourceConnectTable.containsKey(group);
    }

    public void setSsmTranslateTable(Map<IpAddress, Set<IpAddress>> ssmTranslateTable) {
        this.ssmTranslateTable = ssmTranslateTable;
    }

    public void setSinkBlockedTable(Map<Channel, Set<IpAddress>> sinkBlockedTable) {

        this.sinkBlockedTable = sinkBlockedTable;
        mergeSourceSinksTable();
        blockSinks();

    }

    public void blockSinks() {
        sinkBlockedTable.forEach((channel, ipAddresses) -> {
            ipAddresses.forEach(ipAddress -> {
                removeSinks(channel, getHostCP(ipAddress));
            });
        });
    }


    private ConnectPoint getHostCP(IpAddress ip) {
        try {
        return hostService.getHostsByIp(ip).iterator().next().location();

        } catch (Exception e) {
            log.info("execption");
            return null;
        }
    }

    private IpAddress getHostIP(ConnectPoint cp) {
        return hostService.getConnectedHosts(cp).iterator().next().ipAddresses().iterator().next();
    }


}
