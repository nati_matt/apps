/*
 * Copyright 2015-present Open Networking Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.tesissdn.igmp;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferenceCardinality;
import org.apache.felix.scr.annotations.Service;
import org.onlab.packet.Ethernet;
import org.onlab.packet.IGMP;
import org.onlab.packet.IGMPMembership;
import org.onlab.packet.IGMPQuery;
import org.onlab.packet.IPv4;
import org.onlab.packet.Ip4Address;
import org.onlab.packet.IpAddress;
import org.onlab.packet.IpPrefix;
import org.onlab.util.Bandwidth;
import org.onlab.util.SafeRecurringTask;
import org.onlab.util.Tools;

import org.onosproject.cfg.ComponentConfigService;

import org.onosproject.core.ApplicationId;
import org.onosproject.core.CoreService;

import org.onosproject.net.ConnectPoint;
import org.onosproject.net.DeviceId;
import org.onosproject.net.SparseAnnotations;
import org.onosproject.net.behaviour.DefaultQosDescription;
import org.onosproject.net.behaviour.DefaultQueueDescription;
import org.onosproject.net.behaviour.PortConfigBehaviour;
import org.onosproject.net.behaviour.QosConfigBehaviour;
import org.onosproject.net.behaviour.QosDescription;
import org.onosproject.net.behaviour.QosId;
import org.onosproject.net.behaviour.QueueConfigBehaviour;
import org.onosproject.net.behaviour.QueueDescription;
import org.onosproject.net.behaviour.QueueId;
import org.onosproject.net.device.DefaultPortDescription;
import org.onosproject.net.config.ConfigFactory;
import org.onosproject.net.config.NetworkConfigEvent;
import org.onosproject.net.config.NetworkConfigListener;
import org.onosproject.net.config.NetworkConfigRegistry;
import org.onosproject.net.config.basics.SubjectFactories;
import org.onosproject.net.device.DeviceEvent;
import org.onosproject.net.device.DeviceService;

import org.onosproject.net.flow.FlowRuleService;
import org.onosproject.net.flow.TrafficTreatment;
import org.onosproject.net.flow.TrafficSelector;
import org.onosproject.net.flow.DefaultTrafficSelector;
import org.onosproject.net.flow.DefaultTrafficTreatment;

import org.onosproject.net.flowobjective.ForwardingObjective;
import org.onosproject.net.flowobjective.DefaultForwardingObjective;
import org.onosproject.net.flowobjective.FlowObjectiveService;

import org.onosproject.net.host.HostService;
import org.onosproject.net.intent.IntentService;
import org.onosproject.net.link.LinkEvent;
import org.onosproject.net.link.LinkService;

import org.onosproject.net.mcast.McastRoute;
import org.onosproject.net.mcast.MulticastRouteService;

import org.onosproject.net.packet.DefaultOutboundPacket;
import org.onosproject.net.packet.InboundPacket;
import org.onosproject.net.packet.PacketContext;
import org.onosproject.net.packet.PacketProcessor;
import org.onosproject.net.packet.PacketService;

import org.onosproject.net.topology.TopologyEvent;
import org.onosproject.net.topology.TopologyListener;
import org.onosproject.net.topology.TopologyService;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;

import java.nio.ByteBuffer;
import java.util.Map;
import java.util.Set;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Properties;
import java.util.Queue;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.validation.constraints.Null;

import static org.onlab.util.Tools.groupedThreads;
import static org.slf4j.LoggerFactory.getLogger;


/**
 * Internet Group Management Protocol.
 */
@Component(immediate = true)
@Service
public class IgmpSnoopManager implements IgmpSnoopService {
    private static final String APP_NAME = "org.tesissdn.igmp";

    private final Logger log = getLogger(getClass());

    private static final String DEST_MAC = "01:00:5E:00:00:01";
    private static final String DEST_IP = "224.0.0.1";
    private static final byte[] ROUTE_ALERT_BYTES = {(byte) 0x94, (byte) 0x04, (byte) 0x00, (byte) 0x00};
    private static final int DEFAULT_QUERY_PERIOD_SECS = 60;
    private static final byte DEFAULT_IGMP_RESP_CODE = 100;

    @Property(name = "multicastAddress",
            label = "Define the multicast base range to listen to")
    private String multicastAddress = IpPrefix.IPV4_MULTICAST_PREFIX.toString();

    @Property(name = "queryPeriod", intValue = DEFAULT_QUERY_PERIOD_SECS,
            label = "Delay in seconds between successive query runs")
    private int queryPeriod = DEFAULT_QUERY_PERIOD_SECS;

    @Property(name = "maxRespCode", byteValue = DEFAULT_IGMP_RESP_CODE,
            label = "Maximum time allowed before sending a responding report")
    private byte maxRespCode = DEFAULT_IGMP_RESP_CODE;

    @Property(name = "routeAlert", boolValue = false,
            label = "add Route Alert in igmp query header or not")
    private boolean routerAlert = false;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected FlowObjectiveService flowObjectiveService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected PacketService packetService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected CoreService coreService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected NetworkConfigRegistry networkConfig;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected ComponentConfigService componentConfigService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected DeviceService deviceService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected LinkService linkService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    public IntentService intentService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected MulticastRouteService multicastService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected FlowRuleService flowRuleService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected TopologyService topologyService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected HostService hostService;


    protected McastIntentManager mcastIntentManager;


    private InternalTopologyListener topologyListener;

    private ScheduledFuture<?> queryTask;
    private final ScheduledExecutorService queryService =
            Executors.newSingleThreadScheduledExecutor(groupedThreads("onos/igmp-query",
                    "membership-query"));


    private Map<IpAddress, Set<IpAddress>> ssmTranslateTable = new ConcurrentHashMap<>();

    private IgmpPacketProcessor processor = new IgmpPacketProcessor();

    private MulticastData mData;

    private static ApplicationId appId;

    private InternalNetworkConfigListener configListener =
            new InternalNetworkConfigListener();


    private static final Class<IgmpSsmTranslateConfig> SSM_TRANSLATE_CONFIG_CLASS =
            IgmpSsmTranslateConfig.class;


    private ConfigFactory<ApplicationId, IgmpSsmTranslateConfig> ssmTranslateConfigFactory =
            new ConfigFactory<ApplicationId, IgmpSsmTranslateConfig>(
                    SubjectFactories.APP_SUBJECT_FACTORY, SSM_TRANSLATE_CONFIG_CLASS, "sourcesEnabled", true) {
                @Override
                public IgmpSsmTranslateConfig createConfig() {
                    return new IgmpSsmTranslateConfig();
                }
            };

    private static final Class<SinksEnableConfig> SINKS_ENABLE_CONFIG_CLASS =
            SinksEnableConfig.class;


    private ConfigFactory<ApplicationId, SinksEnableConfig> sinksEnableConfigFactory =
            new ConfigFactory<ApplicationId, SinksEnableConfig>(
                    SubjectFactories.APP_SUBJECT_FACTORY, SINKS_ENABLE_CONFIG_CLASS, "sinksBlocked", true) {
                @Override
                public SinksEnableConfig createConfig() {
                    return new SinksEnableConfig();
                }
            };



    private ByteBuffer queryPacket;

    @Activate
    public void activate(ComponentContext context) {
        componentConfigService.registerProperties(getClass());
        modified(context);

        appId = coreService.registerApplication(APP_NAME);

        mcastIntentManager = new McastIntentManager(intentService, appId, multicastService);

        mData = new MulticastData(multicastService, appId, flowRuleService, flowObjectiveService,
                                  hostService);

        networkConfig.registerConfigFactory(ssmTranslateConfigFactory);
        networkConfig.registerConfigFactory(sinksEnableConfigFactory);

        networkConfig.addListener(configListener);

        IgmpSsmTranslateConfig ssmTranslateConfig =
                networkConfig.getConfig(appId, IgmpSsmTranslateConfig.class);

        addToSsmTranslateTable(ssmTranslateConfig.getSsmTranslations());

        SinksEnableConfig sinksEnableConfig =
                networkConfig.getConfig(appId, SinksEnableConfig.class);
        if (sinksEnableConfig != null) {
            mData.setSinkBlockedTable(sinksEnableConfig.getBlocked());
        } else {
            log.info("Block table vacia");
        }

        packetService.addProcessor(processor, PacketProcessor.director(1)); // el numero mas bajo tiene mas prioridad

        topologyListener = new InternalTopologyListener();
        topologyService.addListener(topologyListener);

        restartQueryTask();

        log.info("Started");
    }

    @Deactivate
    public void deactivate() {
        packetService.removeProcessor(processor);
        processor = null;

        topologyService.removeListener(topologyListener);
        networkConfig.removeListener(configListener);

        networkConfig.unregisterConfigFactory(ssmTranslateConfigFactory);
        networkConfig.unregisterConfigFactory(sinksEnableConfigFactory);

        queryTask.cancel(true);
        queryService.shutdownNow();
        componentConfigService.unregisterProperties(getClass(), false);

        mData.clearTables();
        mcastIntentManager.deactivate();

        log.info("Stopped");
    }

    @Modified
    protected void modified(ComponentContext context) {
        Dictionary<?, ?> properties = context != null ? context.getProperties() : new Properties();

        String strQueryPeriod = Tools.get(properties, "queryPeriod");
        String strResponseCode = Tools.get(properties, "maxRespCode");
        String strRouteAlert = Tools.get(properties, "routeAlert");
        try {
            byte newMaxRespCode = Byte.parseByte(strResponseCode);
            if (maxRespCode != newMaxRespCode) {
                maxRespCode = newMaxRespCode;
                queryPacket = buildQueryPacket();
            }

            int newQueryPeriod = Integer.parseInt(strQueryPeriod);
            if (newQueryPeriod != queryPeriod) {
                queryPeriod = newQueryPeriod;
                restartQueryTask();
            }

            boolean newRouteAlert = Boolean.parseBoolean(strRouteAlert);
            if (newRouteAlert != routerAlert) {
                routerAlert = newRouteAlert;
                queryPacket = buildQueryPacket();
            }

        } catch (NumberFormatException e) {
            log.warn("Error parsing config input", e);
        }

        log.info("queryPeriod set to {}", queryPeriod);
        log.info("maxRespCode set to {}", maxRespCode);
    }

    private void restartQueryTask() {
        log.info("-------------------------------> RESTART QUERY TASK");
        if (queryTask != null) {
            queryTask.cancel(true);
        }
        queryPacket = buildQueryPacket();
        queryTask = queryService.scheduleWithFixedDelay(
                SafeRecurringTask.wrap(this::querySubscribers),
                0,
                queryPeriod,
                TimeUnit.SECONDS);
    }

    private void processMembership(IGMP pkt, ConnectPoint location) {
        pkt.getGroups().forEach(group -> {

            if (!(group instanceof IGMPMembership)) {
                log.warn("Wrong group type in IGMP membership");
                return;
            }

            IGMPMembership membership = (IGMPMembership) group;

            IpAddress groupAddress = membership.getGaddr();

            if (membership.getRecordType() == IGMPMembership.MODE_IS_INCLUDE ||
                    membership.getRecordType() == IGMPMembership.CHANGE_TO_INCLUDE_MODE ||
                    membership.getRecordType() == IGMPMembership.ALLOW_NEW_SOURCES) {

                if (membership.getSources().isEmpty()) {
                    if (IpPrefix.valueOf("232.0.0.0/8").contains(groupAddress.toIpPrefix())) {
                        return;
                    }

                    log.info("Unsuscribe ASM -> {}", location);

                    //Aca me desuscribo de todas las sources (ASM)
                    mData.removeSinks(new Channel(group.getGaddr(), null), location);

                } else {
                    if (!IpPrefix.valueOf("232.0.0.0/8").contains(groupAddress.toIpPrefix())) {

                        //Suscripcion SSM a un grupo ASM
                        if (mData.sourceExists(groupAddress)) {
                            log.info("Suscripcion SSM a un grupo ASM");
                            mData.saveSinks(new Channel(groupAddress, null), location);
                        }
                        return;
                    }
                    //Suscripcion SSM
                    if (!ssmTranslateTable.containsKey(groupAddress)) {
                        return;
                    }
                    log.info("Suscribe SSM -> {}", location);
                    membership.getSources().stream()
                            .filter(source -> ssmTranslateTable.get(groupAddress).contains(source))
                            .forEach(source -> mData.saveSinks(new Channel(group.getGaddr(), source), location));
                }

            } else if (membership.getRecordType() == IGMPMembership.MODE_IS_EXCLUDE ||
                    membership.getRecordType() == IGMPMembership.CHANGE_TO_EXCLUDE_MODE ||
                    membership.getRecordType() == IGMPMembership.BLOCK_OLD_SOURCES) {

                if (membership.getSources().isEmpty()) {
                    if (IpPrefix.valueOf("232.0.0.0/8").contains(groupAddress.toIpPrefix())) {
                        return;
                    }
                    //TODO aca va soporte a ASM
                    log.info("Suscribe ASM -> {}", location);
                    mData.saveSinks(new Channel(groupAddress, null), location);
                } else {
                    //Borro sinks SSM
                    if (!IpPrefix.valueOf("232.0.0.0/8").contains(groupAddress.toIpPrefix())) {
                        //Desuscripcion SSM a un grupo ASM
                        log.info("Desuscripcion SSM a un grupo ASM");
                        mData.removeSinks(new Channel(groupAddress, null), location);
                        return;
                    }
                    if (!ssmTranslateTable.containsKey(groupAddress)) {
                        return;
                    }
                    membership.getSources().stream()
                            .filter(source -> ssmTranslateTable.get(groupAddress).contains(source))
                            .forEach(source -> mData.removeSinks(new Channel(group.getGaddr(), source), location));
                    log.info("Unsuscribe SSM -> {}", location);
                }
            }
        });
    }

    private ByteBuffer buildQueryPacket() {
        IGMP igmp = new IGMP.IGMPv3();
        igmp.setIgmpType(IGMP.TYPE_IGMPV3_MEMBERSHIP_QUERY);
        igmp.setMaxRespCode(maxRespCode);

        IGMPQuery query = new IGMPQuery(IpAddress.valueOf("0.0.0.0"), 0);
        igmp.addGroup(query);

        IPv4 ip = new IPv4();
        ip.setDestinationAddress(DEST_IP);
        ip.setProtocol(IPv4.PROTOCOL_IGMP);
        ip.setSourceAddress("192.168.1.1");
        ip.setTtl((byte) 1);
        ip.setPayload(igmp);

        if (routerAlert) {
            ip.setOptions(ROUTE_ALERT_BYTES);
        }

        Ethernet eth = new Ethernet();
        eth.setDestinationMACAddress(DEST_MAC);
        eth.setSourceMACAddress("DE:AD:BE:EF:BA:11");
        eth.setEtherType(Ethernet.TYPE_IPV4);

        eth.setPayload(ip);

        eth.setPad(true);

        return ByteBuffer.wrap(eth.serialize());
    }

    public void querySubscribers() {

        deviceService.getDevices().forEach(device -> {
            deviceService.getPorts(device.id()).stream()
                    .filter(port -> port.isEnabled())
                    .forEach(port -> {
                        ConnectPoint cp = ConnectPoint.deviceConnectPoint(port.element().id().toString() +
                                '/' + port.number().toString());
                        if (linkService.getLinks(cp).isEmpty()) {
                            TrafficTreatment treatment = DefaultTrafficTreatment.builder()
                                    .setOutput(port.number()).build();
                            packetService.emit(new DefaultOutboundPacket((DeviceId) port.element().id(),
                                    treatment, queryPacket));
                        }
                    });
        });
    }

    private void dropMcastPacket(IpAddress src, IpAddress dst, DeviceId devId) {
        log.debug("drop packet");
        TrafficSelector.Builder selector = DefaultTrafficSelector.builder();
        selector.matchEthType(Ethernet.TYPE_IPV4)
                .matchIPProtocol(IPv4.PROTOCOL_UDP)
                .matchIPDst(dst.toIpPrefix())
                .matchIPSrc(src.toIpPrefix());

        TrafficTreatment drop = DefaultTrafficTreatment.builder()
                .drop().build();



        flowObjectiveService.forward(devId,
                                     DefaultForwardingObjective.builder()
                                            .fromApp(appId)
                                             .withSelector(selector.build())
                                             .withTreatment(drop)
                                             //TODO: parametrizar priority
                                             .withPriority(90)
                                             .withFlag(ForwardingObjective.Flag.VERSATILE)
                                             .makePermanent()
                                             .add());
    }

    @Override
    public Map<IpAddress, Set<IpAddress>> getSsmTranslateTable() {
        return ssmTranslateTable;
    }
    
    @Override
    public void addToSsmTranslateTable(List<McastRoute> translations) {
        if (translations != null) {
            // ssmTranslateTable.clear();

            log.info("TRANSLATIONS", translations.toString());
            translations.forEach(route -> {
                if (ssmTranslateTable.containsKey(route.group())) {
                    ssmTranslateTable.get(route.group()).add(route.source());
                } else {
                    Set<IpAddress> sources = new HashSet<>();
                    sources.add(route.source());
                    ssmTranslateTable.put(route.group(), sources);
                }
            });
            log.info("SSM ----------> {}", ssmTranslateTable.toString());
            
            mData.clearTables();
            mData.setSsmTranslateTable(ssmTranslateTable);
        } else {
            log.info("Config vacia :'(");
        }
    }

    /**
     * Packet processor responsible for handling IGMP packets.
     */
    private class IgmpPacketProcessor implements PacketProcessor {

        @Override
        public void process(PacketContext context) {

            InboundPacket pkt = context.inPacket();
            Ethernet ethPkt = pkt.parsed();

            // Stop processing if the packet has been handled, since we
            // can't do any more to it.
            if (context.isHandled()) {
                return;
            }

            if (ethPkt == null) {
                return;
            }

            // IPv6 MLD packets are handled by ICMP6. We'll only deal with IPv4.
            if (ethPkt.getEtherType() != Ethernet.TYPE_IPV4) {
                return;
            }

            IPv4 ip = (IPv4) ethPkt.getPayload();
            IpAddress gaddr = IpAddress.valueOf(ip.getDestinationAddress());
            IpAddress saddr = Ip4Address.valueOf(ip.getSourceAddress());

            if (IpPrefix.IPV4_MULTICAST_PREFIX.contains(saddr)) {
                log.info("IGMP Picked up a packet with a multicast source address.");
                return;
            }

            if (!IpPrefix.IPV4_MULTICAST_PREFIX.contains(gaddr)) {
                return;
            }

            if (!linkService.getLinks(pkt.receivedFrom()).isEmpty()) {
                return;
            }

            if (ip.getProtocol() == IPv4.PROTOCOL_UDP) {
                mData.saveSource(new Channel(gaddr, saddr), pkt.receivedFrom());
                dropMcastPacket(saddr, gaddr, pkt.receivedFrom().deviceId());

            }

            if (ip.getProtocol() == IPv4.PROTOCOL_IGMP) {

                IGMP igmp = (IGMP) ip.getPayload();
                switch (igmp.getIgmpType()) {
                    case IGMP.TYPE_IGMPV3_MEMBERSHIP_REPORT:
                        processMembership(igmp, pkt.receivedFrom());
                        break;

                    case IGMP.TYPE_IGMPV3_MEMBERSHIP_QUERY:
                        log.debug("Received a membership query {} from {}",
                                igmp, pkt.receivedFrom());
                        break;

                    case IGMP.TYPE_IGMPV1_MEMBERSHIP_REPORT:
                    case IGMP.TYPE_IGMPV2_MEMBERSHIP_REPORT:
                    case IGMP.TYPE_IGMPV2_LEAVE_GROUP:
                        log.debug("IGMP version 1 & 2 message types are not currently supported. Message type: {}",
                                igmp.getIgmpType());
                        break;
                    default:
                        log.warn("Unknown IGMP message type: {}", igmp.getIgmpType());
                        break;
                }
            }

            mData.mergeSourceSinksTable();
        }
    }

    private class InternalTopologyListener implements TopologyListener {
        public void event(TopologyEvent event) {
            event.reasons().forEach(event1 -> {
                if (event1.type() == DeviceEvent.Type.DEVICE_AVAILABILITY_CHANGED) {
                    log.info("########### DEVICE_AVAILABILITY_CHANGED {}",
                            event1.subject());

                    mData.clearTables();
                    mcastIntentManager.withdrawAllIntents();
                    querySubscribers();
                }

                if (event1.type() == LinkEvent.Type.LINK_ADDED) {
                    mcastIntentManager.reComputeIntents();
                }

                if (event1.type() == LinkEvent.Type.LINK_REMOVED) {
                    mcastIntentManager.reComputeIntents();
                }
            });
        }
    }

    private class InternalNetworkConfigListener implements NetworkConfigListener {
        @Override
        public void event(NetworkConfigEvent event) {
            switch (event.type()) {

                case CONFIG_ADDED:
                    if (event.configClass().equals(SINKS_ENABLE_CONFIG_CLASS)) {
                        SinksEnableConfig config =
                                networkConfig.getConfig((ApplicationId) event.subject(),
                                                        SINKS_ENABLE_CONFIG_CLASS);
                        log.info("ADD CONFIG {}", config);
                    }
                case CONFIG_UPDATED:
                    if (event.configClass().equals(SSM_TRANSLATE_CONFIG_CLASS)) {
                        IgmpSsmTranslateConfig config =
                                networkConfig.getConfig((ApplicationId) event.subject(),
                                                        SSM_TRANSLATE_CONFIG_CLASS);
                        addToSsmTranslateTable(config.getSsmTranslations());
                        querySubscribers();
                    } else {
                        if (event.configClass().equals(SINKS_ENABLE_CONFIG_CLASS)) {
                            SinksEnableConfig config =
                                    networkConfig.getConfig((ApplicationId) event.subject(),
                                                            SINKS_ENABLE_CONFIG_CLASS);
                            log.info("UPDATE CONFIG {}", config.getBlocked());
                            mData.setSinkBlockedTable(config.getBlocked());
                            querySubscribers();
                        }
                    }
                    break;
                case CONFIG_REGISTERED:
                case CONFIG_UNREGISTERED:
                    break;
                case CONFIG_REMOVED:
                    if (event.configClass().equals(SSM_TRANSLATE_CONFIG_CLASS)) {
                        ssmTranslateTable.clear();
                        mData.clearTables();
                    }
                default:
                    break;
            }
        }
    }

}