/*
 * Copyright 2015-present Open Networking Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.tesissdn.igmp;



import org.onosproject.net.intent.IntentEvent;
import org.onosproject.net.intent.IntentListener;
import org.onosproject.net.intent.IntentService;
import org.onosproject.net.intent.Key;
import org.onosproject.net.intent.SinglePointToMultiPointIntent;

import org.slf4j.Logger;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


import static org.slf4j.LoggerFactory.getLogger;

public class InternalIntentListener  implements IntentListener {
    private final Logger log = getLogger(getClass());
    private Map<Key, SinglePointToMultiPointIntent> intentToInstall;

    private IntentService intentService;

    public InternalIntentListener(IntentService intentService) {
        intentToInstall = new ConcurrentHashMap<>();
        this.intentService = intentService;
    }

    public void addIntentToInstall(Key key, SinglePointToMultiPointIntent sp2mp) {
        intentToInstall.put(key, sp2mp);
    }

    public void clearTable() {
        intentToInstall.clear();
    }

    @Override
    public void event(IntentEvent event) {

        if (event.type() != IntentEvent.Type.WITHDRAWN) {
            return;
        }

        if (!intentToInstall.containsKey(event.subject().key())) {
            return;
        }

        SinglePointToMultiPointIntent intentToSubmit = intentToInstall.get(event.subject().key());

        intentService.submit(intentToSubmit);

        intentToInstall.remove(event.subject().key(), intentToSubmit);

        log.info("-------------- INTENT ADDED -------------- {}", intentToInstall.toString());

    }

}

