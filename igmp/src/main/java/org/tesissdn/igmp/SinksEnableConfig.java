/*
 * Copyright 2016-present Open Networking Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.tesissdn.igmp;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.onlab.packet.IpAddress;
import org.onosproject.core.ApplicationId;
import org.onosproject.net.config.Config;
import org.slf4j.Logger;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import static org.slf4j.LoggerFactory.getLogger;

public class SinksEnableConfig extends Config<ApplicationId> {

    private static final String SOURCE = "source";
    private static final String GROUP = "group";
    private static final String SINKS = "sinks";

    private final Logger log = getLogger(getClass());


    @Override
    public boolean isValid() {
        for (JsonNode node : array) {
            if (!hasOnlyFields((ObjectNode) node, SOURCE, GROUP, SINKS)) {

                return false;
            }

            if (!(isIpAddress((ObjectNode) node, SOURCE, FieldPresence.MANDATORY) &&
                    isIpAddress((ObjectNode) node, GROUP, FieldPresence.MANDATORY))) {
                return false;
            }

        }
        return true;
    }

    public Map<Channel, Set<IpAddress>> getBlocked() {
        Map<Channel, Set<IpAddress>> data = new ConcurrentHashMap<>();
        for (JsonNode node : array) {
            Set<IpAddress> sinks = new HashSet<>();
            if (node.get(SINKS).isArray()) {
                for (final JsonNode objNode : node.get(SINKS)) {
                    sinks.add(IpAddress.valueOf(objNode.asText().trim()));
                }
            } else {
                sinks.add(IpAddress.valueOf(node.path(SINKS).asText().trim()));
            }
            data.put(
                    new Channel(IpAddress.valueOf(node.path(GROUP).asText().trim()),
                                IpAddress.valueOf(node.path(SOURCE).asText().trim())),
                    sinks
            );
        }
        return data;
    }


}
