#!/usr/bin/python

from shutil import copyfile
import json

redirect_file = open('python_redirect.html', 'r')
redirect = redirect_file.readlines()
redirect_file.close()

onos_ip = '192.168.33.101'

headers = {
    'authorization': "Basic c2RuOnJvY2tz",
    'cache-control': "no-cache",
    'postman-token': "3d4497eb-4721-2888-68d0-ae71c7939180"
}

url_devices = "http://" + onos_ip + ":8181/onos/v1/devices"
url_hosts = "http://" + onos_ip + ":8181/onos/v1/hosts"
url_ssm = "http://" + onos_ip + ":8181/onos/v1/network/configuration/apps/org.tesissdn.igmp"
url_mcastroutes = "http://" + onos_ip + ":8181/onos/igmp/sinks/"
url_sinkblocker = "http://" + onos_ip + ":8181/onos/v1/network/configuration/apps/org.tesissdn.igmp"


def update_devices(response):
    copyfile('header.html', 'devices.html')
    dev_file = open('devices.html', 'a')
    dev_file.write('\n <div class="container">')
    dev_file.write('\n <div class="container">')
    dev_file.write('\n <div class="list-group">')

    for device in response['devices']:
        dev_file.write('<a href="#" class="list-group-item">' + device['id'] + ': ' +
                       ('ON' if device['available'] else 'OFF') + '</a> \n')

    dev_file.write("</div> \n </div> \n </body> \n </html>")
    dev_file.close()


def update_hosts(response):
    copyfile('header.html', 'hosts.html')
    hosts_file = open('hosts.html', 'a')
    hosts_file.write('\n <div class="container">')
    hosts_file.write('\n <div class="list-group">')

    for host in response['hosts']:
        try:
            ip = host['ipAddresses'][0]
            hosts_file.write('<a href="#" class="list-group-item">' + ip + '</a> \n')
        except:
            pass

    hosts_file.write("</div> \n </div> \n </body> \n </html>")
    hosts_file.close()


def update_ssm(response):
    copyfile('header.html', 'ssm.html')
    ssm_file = open('ssm.html', 'a')
    ssm_file.write('\n <div class="container"> \n <div class="list-group">')

    try:
        for ssm in response['ssmTranslate']:
            grp = ssm['group']
            src = ssm['source']
            ssm_file.write('<a href="#" class="list-group-item">' + src + ' [' + grp + ']' + '</a> \n')
    except:
        pass
    
    ssm_file.write("</div> \n </div> \n </body> \n </html>")
    ssm_file.close()


def update_mcastroutes(response, response_blocked):
    copyfile('header.html', 'mcastroutes.html')
    mcastroutes_file = open('mcastroutes.html', 'a')
    mcastroutes_file.write('\n <div class="container"> \n <div class="list-group">')
    blocked_sinks = []

    # print response_blocked

    try:
        for index, mcastroute in enumerate(response['mcastRoutes']):
            group = mcastroute['group']
            src = mcastroute['source']
            sinks = mcastroute['sinks']

            try:
                for item in response_blocked['sinksBlocked']:
                    if item['source'] == src and item['group'] == group:
                        blocked_sinks = item['sinks']
            except:
                pass

            # print type(blocked_sinks)

            mcastroutes_file.write('<a href="#" class="list-group-item">'
                                '<p>Grupo: ' + group + '</p>'
                                '<p>Fuente: ' + src + '</p>'
                                '<p>Clientes: ' + ', '.join(sinks) + '</p>'
                                '<p>Bloqueados: ' + ', '.join(blocked_sinks) + '</p>'
                                '</a> \n')
    except:
        pass

    mcastroutes_file.write("</div> \n </div> \n </body> \n </html>")
    mcastroutes_file.close()


def config_source(func, source, group, head):
    resp = requests.request("GET", url_ssm, headers=head)

    response_json = resp.json()

    payload = ""

    if 'ssmTranslate' in response_json:
        if func == 'del':
            for index, ssm_entry in enumerate(response_json['ssmTranslate']):
                if ssm_entry['source'] == str(source) and ssm_entry['group'] == str(group):
                    del response_json['ssmTranslate'][index]
                    payload = json.dumps(response_json)

        elif func == 'add':
            aux_dict = {
                'source': str(source),
                'group': str(group)
            }
            response_json['ssmTranslate'].append(aux_dict)
            payload = json.dumps(response_json)

    response = requests.request("POST", url_ssm, data=payload, headers=headers)

    if response.content == "":
        html_print_error("Done", False)
    else:
        html_print_error(response.content)


def config_client(func, source, group, host, head):

    resp = requests.request("GET", url_sinkblocker, headers=head)

    response_json = resp.json()

    payload = ""

    if 'sinksBlocked' in response_json:
        # print response_json

        if func == 'block':
            if len(response_json['sinksBlocked']) != 0:
                for index, item in enumerate(response_json['sinksBlocked']):
                    if item['source'] == str(source) and item['group'] == str(group):
                        item['sinks'].append(str(host))
            else:
                response_json['sinksBlocked'] = [
                        {
                            "source": str(source),
                            "group": str(group),
                            "sinks": [
                                str(host)
                            ]
                        }
                    ]
            
            payload = json.dumps(response_json)


        elif func == 'unblock':
            for index, item in enumerate(response_json['sinksBlocked']):
                if item['source'] == str(source) and item['group'] == str(group):
                    for sub_index, sub_item in enumerate(item['sinks']):
                        if sub_item == str(host):
                            del item['sinks'][sub_index]

            payload = json.dumps(response_json)

    # print payload
    response = requests.request("POST", url_sinkblocker, data=payload, headers=headers)

    if response.content == "":
        html_print_error("Done", False)
    else:
        html_print_error(response.content)


def html_print_error(msg, error=True):
    for index, line in enumerate(redirect):
        if "<!--alert type-->" in line:
            if error:
                redirect[index] = '<div class ="alert alert-danger" role="alert" >\n'
            else:
                redirect[index] = '<div class ="alert alert-success" role="alert" >\n'
        elif "<!--response message-->" in line:
            redirect[index] = msg+"\n"
        elif "<!--timeout-->" in line:
            if error:
                redirect[index] = '<meta http-equiv="refresh" content="10; url=./main.html" />' + "\n"
            else:
                redirect[index] = '<meta http-equiv="refresh" content="2; url=./main.html" />' + "\n"


if __name__ == '__main__':

    import cgi
    import requests

    form = cgi.FieldStorage()
    # Disable source
    del_mcast_source = form.getvalue("del_source")
    del_mcast_group = form.getvalue("del_group")
    # Enable source
    new_mcast_source = form.getvalue("new_source")
    new_mcast_group = form.getvalue("new_group")
    # Disable client
    dis_sink_mcast_source = form.getvalue("dis_sink_source")
    dis_sink_mcast_group = form.getvalue("dis_sink_group")
    dis_sink_mcast_host = form.getvalue("dis_sink_host")
    # Enable client
    ena_sink_mcast_source = form.getvalue("ena_sink_source")
    ena_sink_mcast_group = form.getvalue("ena_sink_group")
    ena_sink_mcast_host = form.getvalue("ena_sink_host")

    # del_mcast_source = '10.10.0.3'
    # del_mcast_group = '224.0.10.55'

    # dis_sink_mcast_source = '10.10.0.1'
    # dis_sink_mcast_group = '232.0.55.65'
    # dis_sink_mcast_host = '10.10.0.3'

    # ena_sink_mcast_source = '10.10.0.2'
    # ena_sink_mcast_group = '232.0.55.65'
    # ena_sink_mcast_host = '10.10.0.4'

    if del_mcast_source is not None and \
            del_mcast_group is not None:
        config_source('del', del_mcast_source, del_mcast_group, headers)

    elif new_mcast_source is not None and \
            new_mcast_group is not None:
        config_source("add", new_mcast_source, new_mcast_group, headers)

    elif dis_sink_mcast_source is not None and \
            dis_sink_mcast_group is not None and \
            dis_sink_mcast_host is not None:
        config_client('block', dis_sink_mcast_source, dis_sink_mcast_group, dis_sink_mcast_host, headers)

    elif ena_sink_mcast_source is not None and \
            ena_sink_mcast_group is not None and \
            ena_sink_mcast_host is not None:
        config_client("unblock", ena_sink_mcast_source, ena_sink_mcast_group, ena_sink_mcast_host, headers)

    else:
        html_print_error("No Data inserted")

    print ''.join(redirect)

    response = requests.request("GET", url_devices, headers=headers)

    update_devices(response.json())

    response = requests.request("GET", url_hosts, headers=headers)

    update_hosts(response.json())

    response = requests.request("GET", url_ssm, headers=headers)

    update_ssm(response.json())

    response = requests.request("GET", url_mcastroutes, headers=headers)
    response_blocked = requests.request("GET", url_sinkblocker, headers=headers)

    update_mcastroutes(response.json(), response_blocked.json())
